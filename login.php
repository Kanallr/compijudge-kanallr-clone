<?php
   include('./header.html'); 
   ?>
<?php
   session_start(); 
    //Check if user session token is set and create one it not
    if(!isset($_SESSION['user_token']) || isset($_SESSION['user_token'])){
     $token = $_SESSION['user_token'];
     $token = bin2hex(openssl_random_pseudo_bytes(128));
     $_SESSION['user_token'] = $token;
    }
   ?>
<link rel="stylesheet" type="text/css" href="/css/main.css">
<div class="row text-center">
   <h2>Log in</h2>
</div>
<div class="col-md-6" style="float: none; margin: 0 auto;">
   <form name="login" id="login" method="POST" action="authenticate.php" onsubmit="return validate()">
      <div class="col-sm-12 col-md-12">
         <div class="control-group">
            <div class="controls form-inline">
               <input class="control-form input-lg" style="margin-bottom: 3%" type="text" name="username" id="username" placeholder="USERNAME">
            </div>
         </div>
      </div>
      <div class="col-sm-12 col-md-12">
         <div class="control-group">
            <div class="controls form-inline">
               <input class="control-form input-lg" type="password" name="password" id="password" placeholder="PASSWORD">
            </div>
         </div>
      </div>
      <div class="col-sm-12 col-md-12">
         <button type="submit" class="btn-xlarge"  name="submit" id="submit">Submit</button>
      </div>
   </form>
</div>
<script>
   function validate(){
          if(document.getElementById('username').value === "" || document.getElementById('password').value === ""){
   alert("Please enter a username and password.");
   return false;
           }
   if(grecaptcha.getResponse() == ""){
      			alert("Please check reCaptcha box before submitting");
      			return false;
      		}
      }
</script>
</body>
</html>

