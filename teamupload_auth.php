 <?php

$db = new SQLite3('compijudge.db');
if (isset($_POST['submit'])) {
    if (isset($_POST['user-name'])) {
        
        if ($db) {
            $count    = 0;
            //Prevent SQL injection
            $username = SQLite3::escapeString($_POST['user-name']);
            $stmt     = $db->prepare('SELECT ID FROM Users WHERE ID=:ID');
            
            $stmt->bindValue(':ID', $username);
            $result = $stmt->execute();
            while ($row = $result->fetchArray()) {
                $recno = $row['ID'];
                $count++;
            }
            
        }
    }
    
    if (isset($_POST['contest-number'])) {
        $now      = new DateTime();
        $nowdatef = explode(" ", $now->format('Y-m-d H:i:s'));
        $nowdate  = $nowdatef[0];
        
        if ($db) {
            $sql = <<<EOF
                     SELECT StartDate, EndDate FROM Contest 
EOF;
            $ret = $db->query($sql);
            while ($row = $ret->fetchArray(SQLITE3_ASSOC)) {
                if ($row['EndDate'] < $nowdate) {
                    echo " alert('Contest file upload failed: Please select a contest number that has not expired. You will be redirected to submit again.');      
                    window.location.href='teamupload.php';
            </script>";
                }

                if ($row['StartDate'] > $nowdate) {
                    echo " alert('Contest file upload failed: Please select a contest number that is currently available. You will be redirected to submit again.');      
                    window.location.href='teamupload.php';
            </script>";
                }
            }
        }
    }
    //Get the total number of the uploaded files
    $total = count($_FILES['upload']['name']);
    
    $counth   = 0;
    $countcpp = 0;
    $countc   = 0;
    $countjava = 0;
    $countpy = 0;
    $countmain = 0;
    $filearr  = array();
    
    
    //Add uploaded files to the html directory
    for ($i = 0; $i < $total; $i++) {
        
        if (($_FILES['upload']['size'][$i] > 2000000)) {
            echo "<script>";
            echo " alert('Contest file upload failed: Please select a file to upload that is less than 2MB! You will be redirected to submit again.');      
                    window.location.href='teamupload.php';
            </script>";
        }
        
        //File(s) never uploaded
        if (empty($_FILES['upload']['name'][$i])) {
            echo "<script>";
            echo " alert('Contest file upload failed: Please select a file to upload!');      
                    window.location.href='teamupload.php';
            </script>";
        }
        
	$uploaddir = '/var/www/html/';

	if($total == 1){
 		$array     = explode('.', $_FILES['upload']['name'][$i]);
        	$extension = end($array);


		if($extension != "java"){
			$rename[$i]      = $uploaddir . $username . "_" . $_POST['contest-number'] . "." . $extension;
			$destination[$i] = $rename[$i];
			move_uploaded_file($_FILES['upload']['tmp_name'][$i], $destination[$i]);
		}
		if($extension == "java"){

			if($_FILES['upload']['name'][$i] !== $username . "_" . $_POST['contest-number'] . "." . 			$extension){
				 echo "<script>";
            			 echo " alert('Contest file upload failed: Your Java file name must follow the following naming standard: username_contestnumber.java (i.e.: team_1.java)!');      
                   				 window.location.href='teamupload.php';
            			</script>";
			}
			else{
				$destination = $uploaddir . $_FILES['upload']['name'][$i];
				move_uploaded_file($_FILES['upload']['tmp_name'][$i], $destination);

			}
		}
	}

	if($total > 1){
		$rename[$i]      = $uploaddir . $_FILES['upload']['name'][$i];
		$destination[$i] = $rename[$i];
		move_uploaded_file($_FILES['upload']['tmp_name'][$i], $destination[$i]);
	}
    }
    
    //File upload is 5. Alert if a team uploads more than 5 files.
    if ($total > 5) {
        echo "<script>";
        echo " alert('Please do not upload more than five files! The file limit is five.');      
        window.location.href='teamupload.php';
      </script>";
    }
    
    //Get all file extensions.
    for ($i = 0; $i < $total; $i++) {
        
        $array     = explode('.', $_FILES['upload']['name'][$i]);
        $extension = end($array);
        
        if ($extension == "h") {
            $counth++;
            ${'fileh' . $i} = $_FILES['upload']['name'][$i];
            array_push($filearr, ${'fileh' . $i});
        }
        
        if ($extension == "cpp") {
            $countcpp++;
            ${'filecpp' . $i} = $_FILES['upload']['name'][$i];
            array_push($filearr, ${'filecpp' . $i});
        }
        
        if ($extension == "c") {
            $countc++;
	    ${'filec' . $i} = $_FILES['upload']['name'][$i];
            array_push($filearr, ${'filec' . $i});
        }

        if ($extension == "java") {
            $countjava++;
	    ${'filejava' . $i} = $_FILES['upload']['name'][$i];
            array_push($filearr, ${'filejava' . $i});
	    ${'javacontent' . $i} = file_get_contents(${'filejava' . $i});

	    //Obtain the Java file that contains the main method, and rearrange array to place that file in the
	    //first (0th) position
	    if(strpos(${'javacontent' . $i}, 'public static void main') !== false){
		array_unshift($filearr, ${'filejava' . $i});
	    }  
        }

        if ($extension == "py") {
            $countpy++;
	    ${'filepy' . $i} = $_FILES['upload']['name'][$i];
            array_push($filearr, ${'filepy' . $i});
	}

       //Check if uploaded files have main as part of the file name
       if(strpos(${'filepy' . $i}, 'main') === false){
		$countmain++;	
	} 
       
        //No file was uploaded with main as part of the file name
 if($countpy > 0){
	if($countmain === $total){
  		echo "<script>";
        	echo " alert('Please upload one Python file whose file name contains the word main (i.e.: hellomain.py)!');      
            		window.location.href='teamupload.php';
          	      </script>";
	} 
	}
      //One file was uploaded with main as part of the file name
	if($countmain < $total){
		if(strpos(${'filepy' . $i}, 'main') !== false){
		    array_unshift($filearr, ${'filepy' . $i});
		}
	}
    }


    //More than one header file uploaded
    if ($counth > 1) {
        echo "<script>";
        echo " alert('Please do not upload more than one header file!');      
            window.location.href='teamupload.php';
          </script>";
    }
    
    //One header file uploaded, but no cpp or c file upload
    if ((($counth == 1) && ($countcpp == 0)) && (($counth == 1) && ($countc == 0))) {
        echo "<script>";
        echo " alert('Please upload at least C or C++ file that corresponding your uploaded header file!');      
            window.location.href='teamupload.php';
          </script>";
    }
    
    //One or more cpp and c files were uploaded at the same time
    if (($countc >= 1) && ($countcpp >= 1)) {
        echo "<script>";
        echo " alert('Please upload one or more C or C++ file(s), but not both!');      
            window.location.href='teamupload.php';
          </script>";
    }

    //One or more c and java file were uploaded at the same time
    if (($countcpp >= 1) && ($countjava >= 1)) {
        echo "<script>";
        echo " alert('Please upload one or more C++ or Java file(s), but not both!');      
            window.location.href='teamupload.php';
          </script>";
    }


    //One or more c and python file were uploaded at the same time
    if (($countc >= 1) && ($countpy >= 1)) {
        echo "<script>";
        echo " alert('Please upload one or more C or Python file(s), but not both!');      
            window.location.href='teamupload.php';
          </script>";
    }

    //One or more cpp and python file were uploaded at the same time
    if (($countcpp >= 1) && ($countpy >= 1)) {
        echo "<script>";
        echo " alert('Please upload one or more C++ or Python file(s), but not both!');      
            window.location.href='teamupload.php';
          </script>";
    }

    //One or more c and java file were uploaded at the same time
    if (($countc >= 1) && ($countjava >= 1)) {
        echo "<script>";
        echo " alert('Please upload one or more C or Java file(s), but not both!');      
            window.location.href='teamupload.php';
          </script>";
    }

    //One or more java and python file were uploaded at the same time
    if (($countjava >= 1) && ($countpy >= 1)) {
        echo "<script>";
        echo " alert('Please upload one or more Java or Python file(s), but not both!');      
            window.location.href='teamupload.php';
          </script>";
    }

    $comma_separated = implode(" ", $filearr);
    $teamname = $username . "_" . $_POST['contest-number'];
    $contestnum = $_POST['contest-number'];
    
     //Single file uploaded so do not use multiple file Docker script
    if ($total == 1) {
        if ($db) {
            $stmt = $db->prepare('SELECT RecNo FROM Users WHERE ID = :ID');
            $stmt->bindValue(':ID', $username);
            $result = $stmt->execute();
            while ($row = $result->fetchArray()) {
                $recno = $row['RecNo'];
                $db->exec("INSERT INTO Score(RecNo, ID, Round, FileName, Score, Attempt) VALUES ('$recno', '$username', '$contestnum', '','', '0')");
            }
        }


        include('startdocker.php');
//        echo "dockerdone";
    }
    
    //Multiple files uploaded so use multiple file Docker script
    if ($total > 1) {
	if($extension == "c"){
		$docker_command = "echo 'ubuntu' | sudo -S /x/run_multi.sh $comma_separated $teamname > /dev/null &";
		exec($docker_command);
        }
	if($extension == "cpp"){
		$docker_command = "echo 'ubuntu' | sudo -S /x/run_multi.sh $comma_separated $teamname > /dev/null &";
		exec($docker_command);
        }
	if($extension == "java"){
	       $docker_command = "echo 'ubuntu' | sudo -S /x/run_javamulti.sh $comma_separated $teamname > /dev/null &";
		exec($docker_command);
        }

	if($extension == "py"){
	       $docker_command = "echo 'ubuntu' | sudo -S /x/run_pymulti.sh $comma_separated $teamname > /dev/null &";
		exec($docker_command);
        }

        if ($db) {
            $stmt = $db->prepare('SELECT RecNo FROM Users WHERE ID = :ID');
            $stmt->bindValue(':ID', $username);
            $result = $stmt->execute();
            while ($row = $result->fetchArray()) {
                $recno = $row['RecNo'];
                
                $stmt1 = $db->prepare('SELECT Score FROM Score WHERE ID = :ID');
                $stmt1->bindValue(':ID', $username);
                $result1 = $stmt1->execute();
                while ($row1 = $result1->fetchArray()) {
                    /*    if($row1['Score'] == 0 || $row1['Score'] == 1){
                    echo "<script>";
                    echo " alert('Contest file upload failed: Only ONE code file can be submitted per contest! Submission attempts are exhausted.');      
                    window.location.href='teaminterface.php';
                    </script>";
                    }*/
                }
                
                $db->exec("INSERT INTO Score(RecNo, ID, Round, FileName, Score, Attempt) VALUES ('$recno', '$username', '$contestnum', '','', '0')");
                
            }
            include("file.php");

        }
        
    }
}
?> 