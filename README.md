This is a clone a CS 370 project made at SUNY Polytechnic. The goal for our group was to use docker to create an automatic grading system that you can run in a virtual machine and run coding contest securely. 

Primary contributers were:
Anthony Albertina, 
Gerrit Crannell,
Justine Imundo, 
Ryan Kanalley,  
 and “Java” Qiu


This project will likely receive no further updates.