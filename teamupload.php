<?php
include('./logoutheader.html');
  session_start();
  $username =  $_SESSION['TEAMPOST1'];
?>
    
<div class="row">
   <div class="col-md-4 col-md-offset-4 text-center">
      <h2>Contest Submission</h2>
   </div>
</div>
            <h4 class="upload-header">Please at least one file with one of following extensions: c, cpp, java, h, py! Please do not upload more than five files! Files must not be greater than 2MB!</h4>
            <form name="login" id="login" method="POST" enctype="multipart/form-data" action="teamuploadauth.php" onSubmit="return upload();">
         <ul class="col-md-4 col-md-offset-4">
         
               <input type="hidden" name="user-name" id="user-name" value="<?php echo $username ?>"/>
               <label for="contest-number" class="no-label">Contest number:</label>   
               <select name="contest-number" class="no-dropdown">
<?php
  $db = new SQLite3('compijudge.db');
   if($db){
	$sql =<<<EOF
      		SELECT RecNo, StartDate, EndDate FROM Contest 
EOF;
       $ret = $db->query($sql);
       while($row = $ret->fetchArray(SQLITE3_ASSOC) ){
	  $now = new DateTime();  
          $nowdatef = explode(" ", $now->format('Y-m-d H:i:s'));
          $nowdate = $nowdatef[0];

	  if($row['EndDate'] > $nowdate || $row['StartDate'] > $nowdate){
    		echo "<option value='".$row['RecNo']."'>".$row['RecNo']."</option>";
	  }	
   	}
    }

?>
</select>

 		  <input class="upload" id='upload' name="upload[]" type="file" multiple="multiple"/>
	  
            <br/>
            <li style="list-style-type: none">
              <!-- <div class="g-recaptcha" data-sitekey="6LegnSkTAAAAAI2RdJAmkRiGwHXIIdC68f9eYOeS"></div>-->
            </li>
            <br/>
            <li style="list-style-type:none">
               <button type="submit" class="btn-xlarge" name="submit" id="submit">Submit</button>
            </li>
         </ul>
      </form>

   </body>
<script>
      function upload(){
          if(document.getElementById('contest-file').value == ""){ 
              alert("Please upload a file.");
              return false;
          }
           var filename = document.getElementById('contest-file').value
 	   var ext = filename.split('.').pop();
    	   if(ext !== 'cpp' && ext !== 'c' && ext !== 'java' && ext !== 'py'){
		alert("Please upload a file with the extension cpp, c, java or py.");
		return false;
           }
      }
   </script>
   <noscript>Please enable JavaScript. You will be unable to proceed otherwise.</noscript>
</html>
