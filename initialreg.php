<?php
  
  /*
   *  Once an individual successfully completes the initial registration form, add the form information
   *  into the database.
   *  Feature #1
   */
  
  $db = new SQLite3('compijudge.db');

  if(!$db){
    echo 'Not connected to server.  Database connection error. ';
   }
   if($db){
			   //Ensure all POST variables have values before assigning them to variables 
			   if(isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['email']) && isset($_POST['team-name']) && isset($_POST['username']) 
			   && isset($_POST['password'])){

						   //Prevent a SQL injection 
						   $firstname = SQLite3::escapeString($_POST['firstname']);
						   $lastname = SQLite3::escapeString($_POST['lastname']);
						   $email = SQLite3::escapeString($_POST['email']);
						   $teamname = SQLite3::escapeString($_POST['team-name']);
						   $username = SQLite3::escapeString($_POST['username']);
						   $password = SQLite3::escapeString($_POST['password']);
	   	
			$db->exec("INSERT INTO Users(FirstName, LastName, Email, TeamName, ID, Password, Role) VALUES ('$firstname', 
						   '$lastname', '$email', '$teamname', '$username', '$password', 'Team')");				
						  
$sql =<<<EOF
      			SELECT TeamName, ID FROM Users;
EOF;
		$ret = $db->query($sql);
   		while($row = $ret->fetchArray(SQLITE3_ASSOC) ){
			if($row['TeamName'] == $_POST['team-name']){ 
				echo "<script>";
			 	echo "alert('Registration error: Team name already exists. Please select another team name.')
					window.location.href='index.php';
			 	</script>";
			}
			if($row['ID'] == $_POST['username']){ 
				echo "<script>";
			 	echo "alert('Registration error: Username already exists. Please select another username.')
					window.location.href='index.php';
			 	</script>";
			}
		   }

   
	   			if(!empty($_POST['team-name'])){
					$db->exec("INSERT INTO Users(FirstName, LastName, Email, TeamName, ID, Password, Role) VALUES ('$firstname', 
						   '$lastname', '$email', '$teamname', '$username', '$password', 'Team')");		
	
                		}  
	       			if(empty($_POST['team-name'])){

					$db->exec("INSERT INTO Users(FirstName, LastName, Email, TeamName, ID, Password, Role) VALUES ('$firstname', 
						   '$lastname', '$email', '', '$username', '$password', 'Judge')");	

               			}					  
                         	echo "<script>";
			 	echo "alert('Successful registration.')
					window.location.href='index.php';
			 	</script>";
			   }   
    }	
?>