#!/bin/sh

docker create --name Bash1 -i kanallr/ubuntu:v5 bash

docker start Bash1

docker cp /var/www/html/$1.c Bash1:/

docker exec -i Bash1 g++ /$1.c

docker exec -i Bash1 ./a.out > $2.results

docker cp Bash1:/$2.results /var/www/html

docker stop Bash1

docker rm Bash1