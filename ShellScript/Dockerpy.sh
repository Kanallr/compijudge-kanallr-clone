# if we want to add python to docker image that ryan has:
# sudo add-apt-repository ppa:fkrull/deadsnakes
# sudo apt-get update
# sudo apt-get install python2.7

#!/bin/sh
#docker create --name Bash1 -i 22b4ba5439df bash
docker create --name Bash1 -i kanallr/ubuntu:v5 bash
docker start Bash1
#file1=/var/www/html/bin/$1.py
docker cp /var/www/html/$1.py Bash1:/
docker exec -i Bash1 python /$1.py > $1.results
#docker exec -i Bash1  output > $1.results
docker cp Bash1:/$1.results /var/www/html 
docker stop Bash1
docker rm Bash1