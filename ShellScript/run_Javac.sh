#!/bin/bash
# form for the cmd line is the name of the file followed by the file type.
# this should be followed by an argument that holds the team name
# example: ./run_Javac JavaMain.java Java2.java Java3.main TeamName
docker rm Bash1
docker create --name Bash1 -i kanallr/ubuntu:v5 bash
docker start Bash1
if [ "$#" = 0 ]
then
    echo "Bad amount of arguments: too few arguments passed. Did you even try... "
    exit 1
fi
if [ "$#" = 1 ]
 then
    echo "Bad amount of arguments: too few arguments passed. Please pass a file and a team name"
    exit 1
fi
if [ "$#" = 2 ]
    then
        docker cp /var/www/html/$1 Bash1:/
        docker exec -i Bash1 javac /$1
        docker exec -i Bash1 java ${1/%?????/} > $2.results
        docker cp Bash1:/$2.results /var/www/html/
        docker stop Bash1
elif [ "$#" = 3 ]
    then
        docker cp /var/www/html/$1 Bash1:/
        docker cp /var/www/html/$2 Bash1:/
        docker exec -i Bash1 javac $1 $2
        docker exec -i Bash1 java ${1/%?????/} > $3.results
        docker cp Bash1:/$3.results /var/www/html/
        docker stop Bash1
elif [ "$#" = 4 ]
    then
        docker cp /var/www/html/$1 Bash1:/
        docker cp /var/www/html/$2 Bash1:/
        docker cp /var/www/html/$3 Bash1:/
        docker exec -i Bash1 javac $1 $2 $3
        docker exec -i Bash1 java ${1/%?????/} > $4.results
        docker cp Bash1:/$4.results /var/www/html/
        docker stop Bash1
elif [ "$#" = 5 ]
    then
        docker cp /var/www/html/$1 Bash1:/
        docker cp /var/www/html/$2 Bash1:/
        docker cp /var/www/html/$3 Bash1:/
        docker cp /var/www/html/$4 Bash1:/
        docker exec -i Bash1 javac /$1 $2 $3 $4
        docker exec -i Bash1 java ${1/%?????/} > $5.results
        docker cp Bash1:/$5.results /var/www/html/
        docker stop Bash1
elif [ "$#" = 6 ]
    then
        docker cp /var/www/html/$1 Bash1:/
        docker cp /var/www/html/$2 Bash1:/
        docker cp /var/www/html/$3 Bash1:/
        docker cp /var/www/html/$4 Bash1:/
        docker cp /var/www/html/$5 Bash1:/
        docker exec -i Bash1 javac /$1 $2 $3 $4 $5
        docker exec -i Bash1 java ${1/%?????/} > $6.results
        docker cp Bash1:/$6.results /var/www/html/
        docker stop Bash1
elif [ "$#" = 7 ]
    then
        docker cp /var/www/html/$1 Bash1:/
        docker cp /var/www/html/$2 Bash1:/
        docker cp /var/www/html/$3 Bash1:/
        docker cp /var/www/html/$4 Bash1:/
        docker cp /var/www/html/$5 Bash1:/
        docker cp /var/www/html/$6 Bash1:/
        docker exec -i Bash1 javac /$1 $2 $3 $4 $5 $6
        docker exec -i Bash1 java ${1/%?????/} > $7.results
        docker cp Bash1:/$7.results /var/www/html/
        docker stop Bash1
else
        echo "Invalid amount of arguments: Too many arguments passed"
        exit 1
fi
docker rm Bash1