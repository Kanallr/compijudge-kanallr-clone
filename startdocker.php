<?php

  $username = SQLite3::escapeString($_POST['user-name']);
  $contestnum = $_POST['contest-number'];

  $dir = "/var/www/html/upload/";
	if (is_dir($dir)) {
		if ($dh = opendir($dir)) {
				while (($file = readdir($dh)) !== false) {
					if(($file == $username. "_" . $contestnum . ".cpp")){

						$runfile = $username . "_" . $contestnum;

						$docker_command = "echo 'ubuntu' | sudo -S /x/run_cpp.sh $runfile";
						exec($docker_command);
				
						  //Get file from Docker
						  $textfile = $username . "_" . $contestnum . ".result";

						  //Update database with team text file
						  //Connect to database
						  $db = new SQLite3('compijudge.db');
						  if ($db) {
							$username = SQLite3::escapeString($_POST['user-name']);
							$db->exec("UPDATE Score SET FileName='$textfile' WHERE ID='$username'");					
								  //Text file successfully uploaded to file directory, and added to database so call file comparison

								  include('file.php');
						 
							  
									echo "<script>";
									echo " alert('Contest file successfully uploaded.');      
									window.location.href='teaminterface.php';
									</script>";
						  }
						
					}
					if(($file == $username. "_" . $contestnum . ".c")){

						$runfile = $username . "_" . $contestnum;

						$docker_command = "echo 'ubuntu' | sudo -S /x/run_c.sh $runfile";
						exec($docker_command);
				
						  //Get file from Docker
						  $textfile = $username . "_" . $contestnum . ".result";

						  //Update database with team text file
						  //Connect to database
						  $db = new SQLite3('compijudge.db');
						  if ($db) {
							$username = SQLite3::escapeString($_POST['user-name']);
							$db->exec("UPDATE Score SET FileName='$textfile' WHERE ID='$username'");					
								  //Text file successfully uploaded to file directory, and added to database so call file comparison

								  include('file.php');
						 
							  
									echo "<script>";
									echo " alert('Contest file successfully uploaded.');      
									window.location.href='teaminterface.php';
									</script>";
						  }
						
					}
					if(($file == $username. "_" . $contestnum . ".java")){

						$runfile = $username . "_" . $contestnum;

						$docker_command = "echo 'ubuntu' | sudo -S /x/run_java.sh $runfile";
						exec($docker_command);
				
						  //Get file from Docker
						  $textfile = $username . "_" . $contestnum . ".result";

						  //Update database with team text file
						  //Connect to database
						  $db = new SQLite3('compijudge.db');
						  if ($db) {
							$username = SQLite3::escapeString($_POST['user-name']);
							$db->exec("UPDATE Score SET FileName='$textfile' WHERE ID='$username'");					
								  //Text file successfully uploaded to file directory, and added to database so call file comparison

								  include('file.php');
						 
							  
									echo "<script>";
									echo " alert('Contest file successfully uploaded.');      
									window.location.href='teaminterface.php';
									</script>";
						  }
						
					}
					if(($file == $username. "_" . $contestnum . ".py")){

						$runfile = $username . "_" . $contestnum;

						$docker_command = "echo 'ubuntu' | sudo -S /x/run_py.sh $runfile";
						exec($docker_command);
				
						  //Get file from Docker
						  $textfile = $username . "_" . $contestnum . ".result";

						  //Update database with team text file
						  //Connect to database
						  $db = new SQLite3('compijudge.db');
						  if ($db) {
							$username = SQLite3::escapeString($_POST['user-name']);
							$db->exec("UPDATE Score SET FileName='$textfile' WHERE ID='$username'");					
								  //Text file successfully uploaded to file directory, and added to database so call file comparison

								  include('file.php');
						 
							  
									echo "<script>";
									echo " alert('Contest file successfully uploaded.');      
									window.location.href='teaminterface.php';
									</script>";
						  }
						
					}
				}
				closedir($dh);
		}
	}
?>