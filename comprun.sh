#!/bin/sh
docker create --name Bash1 -it d61bd9cdebf9 bash
docker start Bash1
file=/var/www/html/bin/$1.cpp
file2=/var/www/html/bin/$1.c
if [ -f "$file" ];
then
    docker cp /var/www/html/bin/$1.cpp Bash1:/
    docker exec -it Bash1 g++ /$1.cpp
    docker exec -it Bash1 ./a.out > $1.results
    docker cp Bash1:/$1.results /var/www/html/bin


elif [ -f "$file2" ];
then
    docker cp /var/www/html/bin/$1.c Bash1:/
    docker exec -it Bash1 g++ /$1.c
    docker exec -it Bash1 ./a.out > $1.results
    docker cp Bash1:/$1.results /var/www/html/bin
fi

#cp /var/www/html/bin/$1.txt /var/www/html/bin/$2
docker stop Bash1
docker rm Bash1
