<?php
   include('./loginheader.html');
?>
<link rel="stylesheet" type="text/css" href="/css/main.css">
<div class="row">
   <div class="col-md-4 col-md-offset-4 text-center">
      <h1>Secure Coding Contests Made Simple</h1>
   </div>
</div>
<div class="row box-row">
   <div class="col-md-3 text-center" id="centered-p">
      <div class='col-content'>
         <p>Compijudge manages programming contests through an automated and secure web-based system.</p>
      </div>
   </div>
</div>
<div class="row">
	<div class="col-md-4 col-md-offset-4 text-center" id="register">
	   <h2>Get Started</h2>
	</div>
</div>

<div class="col-md-6" id="centered-form">
    <form class="form-horizontal" name="initialregform" id="initialregform" action="initialreg.php" method="POST" onSubmit="return upload();">
      <div class="col-sm-12 col-md-12">
         <div class="control-group">
            <div class="controls form-inline">
               <input class="form-control input-lg" type="text" placeholder="FIRST NAME" name="firstname" id="first-name" title="First name" >
               <input class="form-control input-lg" type="text" placeholder="LAST NAME" name="lastname" id="last-name" title="Last name">
            </div>
         </div>
      </div>
      <div class="col-sm-12 col-md-12">
         <div class="control-group">
            <div class="controls form-inline">
               <input class="form-control input-lg" type="text" name="email" id="email" placeholder="EMAIL" title="Email">
               <input class="form-control input-lg" type="text" name="team-name" id="team-name" placeholder="TEAM NAME" title="Team name">
            </div>
         </div>
      </div>
      <div class="col-sm-12 col-md-12">
         <div class="control-group">
            <div class="controls form-inline">
               <input class="form-control input-lg" type="text" name="username" id="username" placeholder="USERNAME" title="Username">
               <input class="form-control input-lg" type="password" name="password" id="password" placeholder="PASSWORD" title="Password">
            </div>
         </div>
      </div>
<div class="col-sm-12 col-md-12">
<label class="membership-label">Membership Status:</label><br/>
	<label class="radio-inline" id="radio-label"><input type="radio" name="Type" id="Team" value="Team" onclick="javascript:Check();">&nbsp;&nbsp;Team&nbsp;&nbsp;&nbsp;&nbsp;</label>
	<label class="radio-inline" id="radio-label"><input type="radio" name="Type" id="Judge" value="Judge" onclick="javascript:Check();">&nbsp;&nbsp;Judge</label>
</div>


      <div class="col-sm-12 col-md-12">
         <div class="g-recaptcha" data-sitekey="6LegnSkTAAAAAI2RdJAmkRiGwHXIIdC68f9eYOeS"></div>
         <button type="submit" class="btn-xlarge" name="submit" id="submit">Submit</button>
      </div>
   </form>
</div>
<script>
	function Check() {
	    if (document.getElementById('Judge').checked) {
		document.getElementById('team-name').style.display = 'none';
	    }
            	    if (document.getElementById('Judge').checked == false) {
		document.getElementById('team-name').style.display = 'inline';
	    }
	}
</script>
<script>
      function upload(){
         		if(document.getElementById('first-name').value == "" || document.getElementById('last-name').value == "" ||
         		document.getElementById('email').value == "" || 
         		document.getElementById('username').value == "" || document.getElementById('password').value == ""){
         				alert("Please fill out all boxes before submitting.");
         				return false;
         		
         		}
                        //Username validator
 			var re = /^\w+$/;
                        if(document.getElementById('password').value.length != 0 && !re.test(document.getElementById('username').value)) {
                                           alert("Username must contain only letters, numbers and underscores.");
                                           return false;
                        }

         		//Email address validator
                                   if(document.getElementById('email').value != ""){
                                       var validEmail = document.forms["initialregform"]["email"].value;
                                       var atpos = validEmail.indexOf("@");
                                       var dotpos = validEmail.lastIndexOf(".");
         				if(atpos < 1 || dotpost< atpost+2 || dotpos+2 >= validEmail.length){
                                                   alert('Please enter a valid email address.');
                                                   return false;
                                           }
                                   }
     }
</script>
</html>