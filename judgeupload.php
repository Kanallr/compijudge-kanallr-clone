<?php
  include('./logoutheader.html');
?>
        <body>
<link rel="stylesheet" type="text/css" href="/css/main.css">
<div class="row text-center">
  <h2>Make Contest</h2>
</div>
           <h4 class="upload-header">Please enter a contest number, text file containing instructions for the contest, and an output file containing the correct output.</h4>

<div class="col-md-6 col-md-offset-4 column"> 
  <form action="judgeupload_auth.php" method="POST" enctype="multipart/form-data" onsubmit="return upload();">
         <div class="col-sm-12 col-md-12">
            <div class="control-group">
               <div class="controls form-inline">
                  <input class="control-form input-lg" style="margin-bottom:3%" type="text" placeholder="CONTEST NUMBER" name="contest-number" id="contest-number" title="Contest Number">
               </div>
            </div>
         </div>
        <div class="col-sm-12 col-md-12">
            <div class="control-group">
               <div class="controls form-inline">
                 <label for="text-file" class="jtext-file">Text file containing contest problem</label>
                 <input class="file-input" type="file" accept="text/plain" name="text-file" id="text-file"/> 
               </div>
            </div>
         </div>
        <div class="col-sm-12 col-md-12">
            <div class="control-group">
               <div class="controls form-inline">
                 <label for="text-file" class="joutput-file">Correct Contest Output</label>
                 <input class="file-input" type="file" accept="text/plain" name="correct-file" id="correct-file"/> 
               </div>
            </div>
         </div>
       <div class="col-sm-12 col-md-12">
             <table class="judgedate-table">
                  <tr>
                      <td class="start-date">Start Date: &nbsp;</td>
                      <td> 
                         <select id="startday" name="startday"></select>
                         <select id="startmonth" name="startmonth">
				<option value="01">Jan</option>
				<option value="02">Feb</option>
				<option value="03">Mar</option>
				<option value="04">Apr</option>
				<option value="05">May</option>
				<option value="06">Jun</option>
				<option value="07">Jul</option>
				<option value="08">Aug</option>
				<option value="09">Sep</option>
				<option value="10">Oct</option>
				<option value="11">Nov</option>
				<option value="12">Dec</option>
			</select> 
			<select id="startyear" name="startyear"></select>
		    </td>
		</tr>
	    </table>
       </div>
       <div class="col-sm-12 col-md-12">
	    <table class="judgedate-table">
		<tr>
		   <td class="end-date">End Date: &nbsp;&nbsp;&nbsp;</td>
		   <td> 
		     <select id="endday" name="endday"></select>
		     <select id="endmonth" name="endmonth">
			<option value="01">Jan</option>
			<option value="02">Feb</option>
			<option value="03">Mar</option>
			<option value="04">Apr</option>
			<option value="05">May</option>
			<option value="06">Jun</option>
			<option value="07">Jul</option>
			<option value="08">Aug</option>
			<option value="09">Sep</option>
			<option value="10">Oct</option>
			<option value="11">Nov</option>
			<option value="12">Dec</option>
		   </select> 
		   <select id="endyear" name="endyear"></select>
		</td>
	     </tr>
	  </table>
	</div>
	<div class="col-sm-12 col-md-12">
            <button type="submit" class="btn-xlarge"  name="submit" id="submit">Submit</button>
         </div>
      </div>
</form>
</body>
<script>
for(var i = 1; i < 32; i++) {
	var s = i.toString();
	if(s.length == 1) {
		s = "0" + s;
	}
	document.getElementById("startday").innerHTML += ("<option value='" + s + "'>" + i + "  </option>");
        document.getElementById("endday").innerHTML += ("<option value='" + s + "'>" + i + "  </option>");
}
for(var i = new Date().getFullYear(); i < (new Date().getFullYear() + 11); i++) {
	document.getElementById("startyear").innerHTML += ("<option value='" + i + "'>" + i + "  </option>");
	document.getElementById("endyear").innerHTML += ("<option value='" + i + "'>" + i + "  </option>");
}
function ddlValue(id) {
	var e = document.getElementById(id);
	var strUser = e.options[e.selectedIndex].value;
	return strUser;
}
// Validate date
function isDate(ExpiryDate) { // MM/DD/YYYY format
    var objDate,  // date object initialized from the ExpiryDate string 
        mSeconds, // ExpiryDate in milliseconds 
        day,      // day 
        month,    // month 
        year;     // year 
    // date length should be 10 characters (no more no less) 
    if (ExpiryDate.length !== 10) { 
        return false; 
    } 
    // third and sixth character should be '/' 
    if (ExpiryDate.substring(2, 3) !== '/' || ExpiryDate.substring(5, 6) !== '/') { 
        return false; 
    } 
    // extract month, day and year from the ExpiryDate (expected format is mm/dd/yyyy) 
    // subtraction will cast variables to integer implicitly (needed 
    // for !== comparing) 
    month = ExpiryDate.substring(0, 2) - 1; // because months in JS start from 0 
    day = ExpiryDate.substring(3, 5) - 0; 
    year = ExpiryDate.substring(6, 10) - 0; 
    // test year range 
    if (year < 1000 || year > 3000) { 
        return false; 
    } 
    // convert ExpiryDate to milliseconds 
    mSeconds = (new Date(year, month, day)).getTime(); 
    // initialize Date() object from calculated milliseconds 
    objDate = new Date(); 
    objDate.setTime(mSeconds); 
    // compare input date and parts from Date() object 
    // if difference exists then date isn't valid 
    if (objDate.getFullYear() !== year || 
        objDate.getMonth() !== month || 
        objDate.getDate() !== day) { 
        return false; 
    } 
    // otherwise return true 
    return true; 
}
	document.getElementById("submit").onclick = function() {
		var startday = parseInt(ddlValue("startday"));
		var startmonth = parseInt(ddlValue("startmonth"));
		var startyear = parseInt(ddlValue("startyear"));

		var endday = parseInt(ddlValue("endday"));
		var endmonth = parseInt(ddlValue("endmonth"));
		var endyear = parseInt(ddlValue("endyear"));

		// Invalid date
		if(!isDate(ddlValue("startmonth") + "/" + ddlValue("startday") + "/" + ddlValue("startyear"))) {
			document.getElementById("error").innerHTML = "Invalid date";
			return;
		}
		// Invalid date
		if(!isDate(ddlValue("endmonth") + "/" + ddlValue("endday") + "/" + ddlValue("endyear"))) {
			document.getElementById("error").innerHTML = "Invalid date";
			return;
		}	
	}
</script>
   <script>
      function upload(){
           var startdate = (document.getElementById('startyear').value) + '-' + (document.getElementById('startmonth').value) + '-' + (document.getElementById('startday').value);

           var enddate = (document.getElementById('endyear').value) + '-' + (document.getElementById('endmonth').value) + '-' + (document.getElementById('endday').value);

	   var d1 = Date.parse(startdate);
           var d2 = Date.parse(enddate);
           if (d1 > d2) {
                alert ("Judge upload error: Contest start date must be earlier than contest end date. You will be redirected to upload again.");
		return false;
            }
            if(d1 === d2){
                alert ("Judge upload error: Contest start date and end date must not be the same date. You will be redirected to upload again.");
		return false;
	    }

	    //No contest number entered
         if(document.getElementById('contest-number').value === ""){
      		alert("Please enter a contest number");
      		return false;
         }
		 //No text file uploaded
        if(document.getElementById('correct-file').value === "" ){
      		alert("Please upload a text file.");
      		return false;
        }
      	if(document.getElementById('contest-file') != ""){
      		    var allowed = 'txt';
      			var file = document.getElementById('contest-file').value;
      			var extension = file.substr(file.lastIndexOf('.')+1);
				//File not a text file
      			if(extension != allowed){
      				alert("File extension must be txt. Submit again with a txt extension!");
      			}
      			var filesize = document.getElementById('contest-file').files[0].size;
				//File greater than 2MB
      			if(filesize > 2000000){
      				alert("Your file must be smaller than 2 MB. Submit again with a file size of 2MB or less!");
      			}
				//Uploaded file is empty
      			if(filesize === 0){
      				alert("Your file is empty. Submit again with file containing output!")
      			}
      	}
        } 
   </script>
</html>