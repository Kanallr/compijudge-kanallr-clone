#!/bin/sh

# echo $1 --> $1 is the parameter

#compile a program
g++ /var/www/html/upload/$1.cpp -o /var/www/html/upload/$1.out

#remove previous docker container
docker stop compijudge
docker rm compijudge

#create a new container
docker create --name compijudge -it ubuntu bash

docker cp /var/www/html/upload/$1.out compijudge:/$1.out 

docker start compijudge
docker exec -i compijudge /$1.out > $1.result

docker cp compijudge:/$1.result /x/$1.result 

docker stop compijudge
docker rm compijudge