<?php
include('./logoutheader.html');


/*
 * Query the database for new scores and their corresponding team names on an interval of 2 minutes.
 * Display the results in a table.
 */

$db = new SQLite3('compijudge.db');
$count = 0;
if($db){

 $now = new DateTime();  
 $nowdatef = explode(" ", $now->format('Y-m-d H:i:s'));
 $nowdate = $nowdatef[0];

  $query =<<<EOF
        SELECT StartDate, EndDate from Contest
EOF;
  $ret = $db->query($query);
  while($row = $ret->fetchArray(SQLITE3_ASSOC) ){
     if($row['StartDate'] == $nowdate){

$sql =<<<EOF
        SELECT users.TeamName, score.Score FROM Users INNER JOIN Score ON users.ID = score.ID ORDER BY Score DESC;
EOF;
     }
 }
?>

<html>
   <head>
      <link rel="stylesheet" type="text/css" href="/css/main.css">
   </head>
   <body>
      <div class="row text-center">
          <h2>Scoreboard</h2>
      </div>

      <table class="table table-striped">
         <tr>
            <?php
		   echo "<th>Rank</th>";
		   echo "<th>Name</th>";
		   echo "<th>Score</th>";
	     echo "</tr>";
		   $ret = $db->query($sql);
		   while($row = $ret->fetchArray(SQLITE3_ASSOC) ){
		        $count++;
			echo "<tr>";
			    echo "<td>".$count."</td>";
			    echo "<td>".$row['TeamName']. "</td>";
			    echo "<td>".$row['Score']. "</td>";
			echo "</tr>";
   		   }

	echo   "</table>";
   }
        ?>


 <div class="row text-center">
          <h2>Contest Problems</h2>
      </div>

      <table class="table table-striped">
        <tr>
   	   <th class='table-head'>Contest Number</th>
   	   <th class='table-head'>Problems</th>
	   <th class='table-head'>Start Date</th>
   	   <th class='table-head'>End Date</th>
	</tr>
	<?php
		$db = new SQLite3('compijudge.db');
		if($db){

		$sql1 =<<<EOF
      			SELECT Question, Round, StartDate, EndDate FROM Contest;
EOF;
		$ret1 = $db->query($sql1);
   		while($row1 = $ret1->fetchArray(SQLITE3_ASSOC) ){
		 	 echo "<tr>";
				echo "<td>".$row1['Round']. "</td>";
				echo "<td>".$row1['Question']. "</td>";
				echo "<td>" .$row1['StartDate']."</td>";
				echo "<td>".$row1['EndDate']. "</td>";
			echo "</tr>";
   		}
		echo "</table>";
          }
     ?>

   </body>
</html>