<?php
 include('./logoutheader.html');
?>
    <link rel="stylesheet" type="text/css" href="/css/main.css">
   <body>
	<div class="col-sm-12 col-md-12 text-center">
    		<a href="teamupload.php"><button type="submit" class="btn-xlarge" id="btn-space">Submit for Contest</button></a>
    		<a href="scoreboard.php"><button type="submit" class="btn-xlarge">View Results</button></a>
        </div>
	<div class="col-sm-12 col-md-12"><p>&nbsp;&nbsp;&nbsp;&nbsp;</p></div>
	<div class="col-sm-12 col-md-12"><p>&nbsp;&nbsp;&nbsp;&nbsp;</p></div>
	<div class="col-sm-12 col-md-12"><p>&nbsp;&nbsp;&nbsp;&nbsp;</p></div>
	<div class="col-sm-12 col-md-12 text-center">
		<h3>Please email judges at compijudge@gmail.com with any contest questions.</h3>
	</div>	
   </body>
   <!--Check if the token is valid every 10 minutes-->
	<script>
		  $(document).ready(function(){
					setInterval(function() {
					   $.get('check_session.php', function(data) {
					   if(data !== ""){
						 alert(data);
					   }
					   if(data === 'Your session has expired'){
							  //Redirect to login page
							  window.location.href="login.php";
						}
					});
				}, 600000);
		   });
	</script>
	<!--Invalid token every 30 minutes-->
	<script>
		  $(document).ready(function(){
					setInterval(function() {
					   $.get('destroy_session.php', function(data) {
						alert(data);
						  if(data === 'Your session has expired'){
							  //Redirect to login page
							  window.location.href="login.php";
						}
				
					});
				}, 1800000);
		   });
	</script>
</html>