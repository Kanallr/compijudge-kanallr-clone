<?php
/*
 *  Authenticate individual based on username and password entered in the log in page.
 *  If team credentials, redirect to team interface. If judge credentials, redirect to
 *  judge interface. If credentials are not in the database, redirect to log in again.
 *  Feature #1
 */


if(isset($_POST['username']) && isset($_POST['password'])) { 
  $db = new SQLite3('compijudge.db');
 if($db){
		session_start();

		//Prevent against SQL injection
		$myusername = SQLite3::escapeString($_POST['username']);
		$mypassword= SQLite3::escapeString($_POST['password']);


		$count = 0;
$sql =<<<EOF
      SELECT ID, Password, Role FROM Users WHERE ID= :ID and Password = :Password 
EOF;

		$ret = $db->prepare($sql);
		$ret->bindValue(':ID', $myusername);
		$ret->bindValue(':Password', $mypassword);
		$result = $ret->execute();
				
		if ($res = $result->fetchArray(SQLITE3_ASSOC)) {
			foreach ($res as $item) {
				$count++;
				$role =  $res['Role'];
				echo $role;
			}
		}

		//Username and password in database
		if($count > 0){
			//Determine if they are a judge or team and redirect to appropriate interface
			if($role === 'Judge' || $role === "Judge"){
				header("Location: judgeinterface.html");
				exit();
				$_SESSION['POSTDATA']=$_POST['username'];
				$_SESSION['POSTDATA2'] = $_POST['password'];
			}
			if($role === 'Team' || $role === "Team"){
				header("Location: teaminterface.html");
				exit();
				$_SESSION['TEAMPOST1']=$_POST['username'];
	            $_SESSION['TEAMPOST2']= $_POST['password'];
			}
		
		}
	
		//Username and/or password not in database 
	else{	
			echo "<script>";
			echo "alert('Username and/or password are incorrect. Please log in again. If you do not have an account, register for free today!');      
					window.location.href='login.html';
			</script>";
		}
	}
}


?>