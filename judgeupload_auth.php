<?php	
   /*
    *  Once a judge successfully completes the contest upload form, ensure the javascript validation
    *  correctly validated by checking for all fields and their specific requirements again. If the validation
    *  passes again, add the file to the upload directory, and redirect to the judge interface. If the validation
    *  fails, redirect to the contest upload form to submit again.
    */

	if (isset($_POST['submit'])) {
		if(isset($_POST['contest-number'])){
			//Prevent SQL injections
			$contestnum = SQLite3::escapeString($_POST['contest-number']);

			$db = new SQLite3('compijudge.db');
			if($db){	
				$sql =<<<EOF
      				SELECT RecNo FROM Contest;
EOF;
				$ret = $db->query($sql);
   				while($row = $ret->fetchArray(SQLITE3_ASSOC) ){
					if($row['RecNo'] == $contestnum){
					    echo "<script>";
					    echo " alert('Judge contest file upload failed: Please enter another contest number. The contest number you entered already exists. You will be redirected to submit again.');      
					window.location.href='judgeupload.php';
			</script>";
					}
   				}
			}
		}
		//No contest number was provided.
		if(empty($_POST['contest-number'])){
			echo "<script>";
			echo " alert('Judge contest file upload failed: Please enter the contest number! You will be redirected to submit again.');      
					window.location.href='judgeupload.php';
			</script>";
		}

		if(empty($_FILES['text-file']['name'])){
			echo "<script>";
			echo " alert('Judge contest file upload failed: Please upload a text file! You will be redirected to submit again.');      
					window.location.href='judgeupload.php';
			</script>";

		}
		if(isset($_FILES['correct-file']['name'])){
			$contestfile=($_FILES['correct-file']['name']);
			$ext = end((explode(".", $contestfile)));
			$allowed = 'result';

			//File is larger than 2MB
			if($_FILES['correct-file']['size'] > 2000000){
			echo "<script>";
			echo " alert('Judge contest file upload failed: Please select a file to upload that is less than 2MB! You will be redirected to submit again.');      
					window.location.href='judgeupload.php';
			</script>";
			}
			//File is completely empty
			if($_FILES['correct-file']['size'] === 0){
			echo "<script>";
			echo " alert('Judge contest file upload failed: Please select a file that contains output! This file to empty! You will be redirected to submit again.');     
					window.location.href='judgeupload.php';
			</script>";
			}

			
			//$textfile = file_get_contents($_FILES['text-file']['name']);

			//Create new file name based on contest number 
			//$extension = '.result';
			$createdfile = 'judge' . '_' . $contestnum . '.' . $ext;	
			$uploaddir = '/var/www/html/';
			$rename = $uploaddir . $createdfile;
			$destination = $rename;  
			move_uploaded_file($_FILES['correct-file']['tmp_name'], $destination);

			$test = 'judge' . '_' . $contestnum;
			if(file_exists($test . '.c')){
				$docker_command = "echo 'ubuntu' | sudo -S /x/run_c.sh $test > /dev/null &";
				exec($docker_command);
			}
			if(file_exists($test . ".cpp")){
				$docker_command = "echo 'ubuntu' | sudo -S /x/run_cpp.sh $test > /dev/null &";
				exec($docker_command);
			}
			if(file_exists($test . '.java')){
				$docker_command = "echo 'ubuntu' | sudo -S /x/run_java.sh $test > /dev/null &";
				exec($docker_command);
			}
			if(file_exists($test . '.py')){
				$docker_command = "echo 'ubuntu' | sudo -S /x/run_py.sh $test > /dev/null &";
				exec($docker_command);
			}
			if(file_exists($test . '.result')){
				//Do not attempt to compile and execute with Docker as this file is a text file!
			}


			$extension1 = '.question';
			$createdfile1 = $contestnum . $extension1;	
			$uploaddir = '/var/www/html/';
			$rename1 = $uploaddir . $createdfile1;
			$destination1 = $rename1;  
			move_uploaded_file($_FILES['text-file']['tmp_name'], $destination1);

			echo "<script>";
			echo " alert('Judge contest file successfully uploaded.');      
					window.location.href='judgeupload.php';
			</script>";			
		}

		if(isset($_FILES['text-file']['name'])){
			$textfile=($_FILES['text-file']['name']);

			$ext = end((explode(".", $textfile)));
			$allowed = 'question';
			//File extension is not .result
			if($ext !== $allowed){
			echo "<script>";
			echo " alert('Judge contest file upload failed: Please select a file that contains a question file extension (ie: file.question)! You will be redirected to submit again.');      
					window.location.href='judgeupload.php';
			</script>";
			}


			//File is larger than 2MB
			if($_FILES['text-file']['size'] > 2000000){
			echo "<script>";
			echo " alert('Judge contest file upload failed: Please select a file to upload that is less than 2MB! You will be redirected to submit again.');      
					window.location.href='judgeupload.php';
			</script>";
			}

			//File is completely empty
			if($_FILES['text-file']['size'] === 0){
			echo "<script>";
			echo " alert('Judge contest file upload failed: Please select a file that contains output! This file to empty! You will be redirected to submit again.');     
					window.location.href='judgeupload.php';
			</script>";
			}
		
		$startdate = $_POST['startyear'] . '-' . $_POST['startmonth'] . '-' . $_POST['startday'];
		$enddate = $_POST['endyear'] . '-' .$_POST['endmonth'] . '-' . $_POST['endday'];


		$textfinal = file_get_contents($textfile);
		$outputfile = file_get_contents('judge_1.results');

		$db = new SQLite3('compijudge.db');
			if($db){
			$db->exec("INSERT INTO Contest (RecNo, Round, Question, Answer, StartDate, StartTime, EndDate, EndTime) VALUES ('$contestnum', '$contestnum', '$textfinal', '$outputfile', '$startdate', '', '$enddate', '')");	
			echo "<script>";
			echo " alert('Judge contest file successful');      
					window.location.href='judgeupload.php';
			</script>";
			}

			echo "<script>";
			echo " alert('Judge contest file successfully uploaded.');      
					window.location.href='judgeupload.php';
			</script>";			
		}
	}
?>