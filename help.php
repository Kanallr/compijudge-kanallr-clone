<?php
 include('./logoutheader.html');
?>

<html>
   <head>
      <title>Compijudge | Help</title>
   </head>
   <body>
      <div class="row text-center">
         <h1>Help</h1>
      </div>
      <div class="row" id="team-name-row">
         <h2>Team</h2>
      </div>
    <div class="help-col-div">
      <div class="col-md-4" id="help-col">
         <div class="row text-center">
            <h3>Team Registration</h3>
         </div>
	<iframe width="320" height="315" src="https://www.youtube.com/embed/gvLYPzcRAXo" frameborder="0" allowfullscreen>
       </iframe>
       <p>To register as a team, navigate to the registration form on the home page. Select the 'Team' membership    		  status. Complete all fields in the form. Click the submit button when done.</p>
      </div>
      <div class="col-md-4" id="help-col">
         <div class="row text-center">
            <h3>Team Login</h3>
         </div>
	<iframe width="320" height="315" src="https://www.youtube.com/embed/C34I3sU8yrc" frameborder="0" allowfullscreen>
	</iframe>
       <p>To login as an existing team, navigate to the 'Login' button located in the top righthand corner of the home page. Select the button. Complete all fields in the form. Click the submit button when done.
      </p>
      </div>
</div>
<div class="col-sm-12 col-md-12"><p>&nbsp;&nbsp;&nbsp;&nbsp;</p></div>
<div class="col-sm-12 col-md-12"><p>&nbsp;&nbsp;&nbsp;&nbsp;</p></div>
<div class="col-sm-12 col-md-12"><p>&nbsp;&nbsp;&nbsp;&nbsp;</p></div>

 <div class="help-col-div">
  <div class="col-md-4" id="help-col">
         <div class="row text-center">
            <h3>Team Contest Submission</h3>
         </div>
	<iframe width="320" height="315" src="https://www.youtube.com/embed/5ozlBgQ_Q4c" frameborder="0" allowfullscreen>
       </iframe>
       <p>To submit code for a contest, navigate to the 'Submit for Contest' button located in the team home page. Select the button. Once on the contest submission page, select the contest number for which you would like to submit. The file extension must be .c, .cpp, .java or .py. After, click the 'Browse' button to select the code file that you would like to submit. Click the submit button when done.
       </div>
   <div class="col-md-4" id="help-col">
         <div class="row text-center">
            <h3>Team Scoreboard & Problems</h3>
         </div>
	<iframe width="320" height="315" src="https://www.youtube.com/embed/CJtBncFR_5o" frameborder="0" allowfullscreen>
       </iframe>
       <p>To view problems and team rankings for current contests, navigate to the 'View Results' button located in the team home page. The scoreboard shows team ranking numbers in descending order along with the corresponding team's name and score for the current contest. The problems list shows current contests' numbers, problems, and start and end dates.
       </div>
   </div>
 </div>

<div class="col-sm-12 col-md-12"><p>&nbsp;&nbsp;&nbsp;&nbsp;</p></div>
<div class="col-sm-12 col-md-12"><p>&nbsp;&nbsp;&nbsp;&nbsp;</p></div>
<div class="col-sm-12 col-md-12"><p>&nbsp;&nbsp;&nbsp;&nbsp;</p></div>

      <div class="col-lg-12 col-md-12" id="judge-name-row">
         <h2>Judge</h2>
      </div>
   <div class="help-col-div">
      <div class="col-md-4" id="help-col">
         <div class="row text-center">
            <h3>Judge Registration</h3>
         </div>
	<iframe width="320" height="315" src="https://www.youtube.com/embed/jNBHRz65VFY" frameborder="0" allowfullscreen>
       </iframe>
       <p>To register as a judge, navigate to the registration form on the home page. Select the 'Judge' membership    		  status. Complete all fields in the form. Click the submit button when done.</p>
      </div>
        <div class="col-md-4" id="help-col">
         <div class="row text-center">
            <h3>Judge Login</h3>
         </div>
	<iframe width="320" height="315" src="https://www.youtube.com/embed/R_jK7HWBC8o" frameborder="0" allowfullscreen>
       </iframe>
       <p>To login as an existing judge, navigate to the 'Login' button located in the top righthand corner of the home page. Select the button. Complete all fields in the form. Click the submit button when done.
       </div>
   </div>
    </div>

<div class="col-sm-12 col-md-12"><p>&nbsp;&nbsp;&nbsp;&nbsp;</p></div>
<div class="col-sm-12 col-md-12"><p>&nbsp;&nbsp;&nbsp;&nbsp;</p></div>
<div class="col-sm-12 col-md-12"><p>&nbsp;&nbsp;&nbsp;&nbsp;</p></div>

  <div class="help-col-div">
      <div class="col-md-4" id="help-col">
         <div class="row text-center">
            <h3>Judge Contest Creation</h3>
         </div>
	<iframe width="320" height="315" src="https://www.youtube.com/embed/hSYCnnYcRc0" frameborder="0" allowfullscreen>
       </iframe>
       <p>To create a contest, navigate to the 'Make Contest' button located in the judge home page. Select the button. Once on the contest creation page, enter the contest number for which you would to create. After, click the 'Browse' button with the label 'Text file containing contest problem' to select the text file that you would like to submit which contains the instructions for the contest. That file extension must be .question. Then, click the 'Browse' button with the label 'Correct Contest Output' to select the text file that you would like to submit which contains the correct output corresponding to the contest problem. That file extension must be .result. Click the submit button when done.</p>
      </div>
        <div class="col-md-4" id="help-col">
         <div class="row text-center">
            <h3>Scoreboard & Problems</h3>
         </div>
	<iframe width="320" height="315" src="https://www.youtube.com/embed/R_jK7HWBC8o" frameborder="0" allowfullscreen>
       </iframe>
       <p>To view problems and team rankings for current contests, navigate to the 'View Scores' button located in the team home page. The scoreboard shows team ranking numbers in descending order along with the corresponding team's name and score for the current contest. The problems list shows current contests' numbers, problems, and start and end dates.
       </div>
   </div>
    </div>
   </body>
</html>

