<?php
  include('./logoutheader.html');
?>
   <body>
      <div class="row text-center">
       <a href="judgeupload.php"><button type="submit" class="btn-xlarge" id="btn-space">Make Contest</button></a>
       <a href="scoreboard.php"><button type="submit" class="btn-xlarge">View Scores</button></a>
      </div>
   </body>
   <!--Check if the token is valid every 10 minutes-->
	<script>
		  $(document).ready(function(){
					setInterval(function() {
					   $.get('check_session.php', function(data) {
					   if(data !== ""){
						 alert(data);
					   }
					   if(data === 'Your session has expired'){
							  //Redirect to login page
							  window.location.href="login.php";
						}
					});
				}, 600000);
		   });
	</script>
	<!--Invalid token every 30 minutes-->
	<script>
		  $(document).ready(function(){
					setInterval(function() {
					   $.get('destroy_session.php', function(data) {
						alert(data);
						  if(data === 'Your session has expired'){
							  //Redirect to login page
							  window.location.href="login.php";
						}
				
					});
				}, 1800000);
		   });
   </script>
</html>